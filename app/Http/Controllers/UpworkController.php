<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\UpworkJobPost;
use Illuminate\Http\Request;
use Validator, Input, Redirect; 
use App\Helpers\JSONHelper;
use App\Agents\JobAgent;
use Illuminate\Support\Facades\DB;

class UpworkController extends Controller {

	public function posting(Request $request) {
        $jobPostingArray = $request->input('job_posting');

        if (strlen($jobPostingArray) > 1) {
           $jobPostingJSONArray = JSONHelper::json_validate($jobPostingArray);
        } else {
            return json_encode([
                'message' => 'Invalid JSON. Expected { "job_posting": [] }'
            ]);
        }

        $numPost = count($jobPostingJSONArray);
        $validPost = $numPost;
        $badPostTitle = [];

        foreach ($jobPostingJSONArray as $key => $row) {
            /*if(UpworkJobPost::isValidJobPost($row)) {
                $validPost--;
                $badPostTitle[] = 'Post # ' . $key . ' was missing expected fields ' . implode(',' , array_keys($row));
                continue;
            }*/

            $jobPostExist = UpworkJobPost::where('url', 'LIKE', '%' . $row['url'] . '%')->get();
            
            if ($jobPostExist && count($jobPostExist) > 0) {
                $validPost--;
                $badPostTitle[] = 'Post # ' . $key . ' ' . $row['url'] . 
                ' already exist in system';     
                continue;               
            }

            $jobPost = new UpworkJobPost;
            $jobPost->title = $row['title'];
            $jobPost->url = $row['url'];
            $jobPost->country = $row['country'] ? $row['country'] : '';
            $jobPost->description = $row['description'];
            $jobPost->type = $row['type'];
            $jobPost->budget = $row['budget'];
            $jobPost->time = $row['time'];
            $jobPost->processed = 0;
            $jobPost->category = 'all';
            $jobPost->save();
        }
        $this->processPost();
        return json_encode([
            'valid_post' => $validPost,
            'messages'   => $badPostTitle,
            'success'    => ($validPost == $numPost) ? true : false
        ]);
	}

    private function processPost() {
        $agent = new JobAgent();
        $jobPosts = UpworkJobPost::where('processed', 0)->orderBy('id', 'desc')->take(200)->get();
        $result = $agent->filterJobs($jobPosts);
        foreach ($result['jobs'] as $key => $value) {
            $value->save();
        }
    }
    public function posting2() {
        $agent = new JobAgent();
        $jobPosts = UpworkJobPost::where('category', 'all')->orderBy('id', 'desc')->take(600)->get();
        $result = $agent->filterJobs($jobPosts);
        foreach ($result['jobs'] as $key => $value) {
            $value->save();
        }
       return 1;
    }

    public function reprocessPost(Request $request) {
        $id = $request->input('id', -1);
        $agent = new JobAgent();
        $jobPosts = UpworkJobPost::where('id', $id)->get();
        $result = $agent->filterJobs($jobPosts);
        foreach ($result['jobs'] as $key => $value) {
            $value->save();
        }
        return '{"success": true}';
    }

    public function cancelPost(Request $request) {
        $id = $request->input('id', -1);
        $jobPost = UpworkJobPost::where('id', $id)->first();
        if($jobPost) {
         $jobPost->processed = 1;
         $jobPost->save();
         return '{"success": true}';
        } else {
         return '{"success": false}';
        }
    }

    public function processReport(Request $request) {
        $id = $request->input('id', -1);
        $jobPost = UpworkJobPost::where('id', $id)->first();
        if($jobPost) {
         $jobPost->processed = 2;
         $jobPost->save();
         return '{"success": true}';
        } else {
         return '{"success": false}';
        }
    }

    public function categoryReport(Request $request) {
        $rows = DB::select( DB::raw('SELECT DATE(created_at) as date, category, count(*) as amount FROM tb_upwork_post GROUP BY DATE(created_at), category'));
        $result = [];
        $currentDate = '';  
        
        foreach ($rows as $key => $value) {
            if ($value->date != $currentDate) {
                $currentDate = $value->date;
                $result[$currentDate] = [];
            }

            $result[$currentDate][][$value->category] = $value->amount;
        }
        return json_encode($result);
        
    }
}