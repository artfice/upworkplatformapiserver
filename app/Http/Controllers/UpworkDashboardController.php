<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\UpworkJobPost;
use App\UpworkJobCategory;
use Illuminate\Http\Request;
use Validator, Input, Redirect;
use App\Helpers\JSONHelper;
use Illuminate\Support\Facades\DB;

class UpworkDashboardController extends Controller {
	
	public function index(Request $request) {
         $category = $request->input('category', 'all');
         $process = $request->input('process', 0);
         if ($process != 0) {
             $this->processExistingJobs($category);
         }
        $categories = UpworkJobCategory::all();
        $categorySelected = UpworkJobCategory::where('slug', $category)->first();
        $jobPosts = UpworkJobPost::where('category', $category)->where('processed', 0)->orderBy('time', 'desc')->take(150)->get();
        return view('upworkdashboard', [
            'jobs' => $jobPosts,
            'categories' => $categories,
            'urls' => $this->_extractUrlFromJob($jobPosts),
            'selectedCategory' => $categorySelected
        ]);
	}

    private function _extractUrlFromJob ($jobs) {
        $result = [];
        foreach ($jobs as $key => $job) {
            $result[] = str_replace(['/jobs/_', '/'], ['', ''], $job->url);
        }

        return $result;
    }

    private function processExistingJobs($category) {
        $jobPosts = UpworkJobPost::where('category', $category)->where('processed', 0)->get();
        foreach ($jobPosts as $key => $job) {
            $job->processed = 1;
            $job->save();
        }
    }

	public function search(Request $request) {
         $title = $request->input('title', '');
         $url = $request->input('url', '');
         $country = $request->input('country', '');
         $category = $request->input('category', '');
         $budget = $request->input('budget', '');
         $process = $request->input('processed', '');
         $categories = UpworkJobCategory::all();
         if (strlen($title) > 0) {
             $jobPosts = UpworkJobPost::where('title', $title);
         }
         if (strlen($url) > 0) {
             $jobPosts = UpworkJobPost::where('url', $url);
         }
         if (strlen($country) > 0) {
             $jobPosts = UpworkJobPost::where('country', $country);
         }
         if (strlen($category) > 0) {
             $jobPosts = UpworkJobPost::where('category', $category);
         }
         if (strlen($budget) > 0) {
             $jobPosts = UpworkJobPost::where('budget', $budget);
         }
         if (strlen($process) > 0) {
             $jobPosts = UpworkJobPost::where('processed', $process);
         }
         if (isset($jobPosts)) {
           $jobPosts = $jobPosts->orderBy('time', 'desc')->get();
         } else {
           $jobPosts = [];
         }
        return view('upworksearch', [
            'jobs' => $jobPosts,
            'categories' => $categories
        ]);
	}

	public function mainIndex(Request $request) {
        return view('upworkmain', [
        ]);
	}
}
