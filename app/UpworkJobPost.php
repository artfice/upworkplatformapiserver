<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpworkJobPost extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tb_upwork_post';
    static function isValidJobPost ($jobPost) {
        error_log( '<pre>' . print_r($jobPost, true) . '</pre>');
        return array_key_exists('title', $jobPost) &&
               array_key_exists('url', $jobPost) &&
               array_key_exists('description', $jobPost) &&
               array_key_exists('type', $jobPost) &&
               array_key_exists('budget', $jobPost) &&
               array_key_exists('time', $jobPost);
    }
}