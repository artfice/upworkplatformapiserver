<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpworkJobCategory extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tb_upwork_category';
}