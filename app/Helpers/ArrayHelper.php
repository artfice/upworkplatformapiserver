<?php namespace App\Helpers;

class ArrayHelper {
    static function top($list) {
       $topValue = reset($list);
       $topKey = null;

       foreach ($list as $key => $value) {
           if ($value >= $topValue) {
                $topValue = $value;
                $topKey = $key;
           }          
       }
       return [
          'key' => $topKey,
          'value' => $topValue
       ];
   }
}