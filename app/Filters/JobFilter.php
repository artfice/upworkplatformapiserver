<?php namespace App\Filters;

use App\Helpers\ArrayHelper;

class JobFilter {
	private $_jobs = [];
	private $_rules = [];
	
	public function __construct() {
	}
	
	public function getInitialScore () {
		$scores = [];
		
		foreach ($this->_rules as $key => $value) {
			$scores[$value::CATEGORY] = 0;
		}
		
		return $scores;
	}
	
	public function addRule ($rule) {
		$this->_rules[] = $rule;
	}
	
	public function process($jobs) {
		$numJobs = count($jobs);
		$correctlyFilter = $numJobs;
		$failedFilterMessages = [];
		foreach ($jobs as $key => $job) {
		   $result = $this->processJob($job);
	       	   if ($result == 'all') {
			$failedFilterMessages[] = 'unfilled job ID' . $job->id;
			$correctlyFilter--;
		   } else {
                        $jobs[$key]->category = $result;
                   }
	        }
		
		return [
		    'num_job' => $numJobs,
                    'correct' => $correctlyFilter,
                    'message' => $failedFilterMessages,
                    'jobs' => $jobs
                ];
	}
	
	public function processJob($job) {
		$score = $this->getInitialScore();
		foreach ($this->_rules as $key => $value) {
                        $currentJob = clone $job; 
                        $currentJob->title = ' ' . $currentJob->title;
                        $currentJob->description = ' ' . $currentJob->description;
			$result = $value::score($currentJob);
                        $score[$value::CATEGORY] = $result;
		}
                $topCategory = ArrayHelper::top($score);
                $category = $topCategory['key'];
                if ($topCategory['value'] < 1) {
                  $category = 'all';
                }
                //echo '<pre>' . print_r($score, true) . '</pre>';
                return $category;
	 }
}

