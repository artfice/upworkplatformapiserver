<?php namespace App\Filters\Rules;

class IonicRule {

    const CATEGORY = 'ionic';

    public static function score ($job) {
        $score = -10;
        if (stripos($job['title'], 'ionic')) {
            return 10000;
        }
        if (stripos($job['title'], 'React Native')) {
            return 10000;
        }
        if (stripos($job['title'], 'ReactNative')) {
            return 10000;
        }
        if (stripos($job['title'], 'React-Native')) {
            return 10000;
        }

        if (stripos($job['title'], 'Titanium app')) {
            return 10000;
        }
        if (stripos($job['title'], 'hybrid app')) {
            return 10000;
        }
        if (stripos($job['title'], 'cordova')) {
            return 10000;
        }
        if (stripos($job['title'], 'Xamarin')) {
            return 10000;
        }

        if (stripos($job['description'], 'ionic')) {
            $score = $score + 2000;
        }
        
        if (stripos($job['description'], 'Titanium app')) {
            $score = $score + 2000;
        }
        if (stripos($job['description'], 'React Native')) {
            $score = $score + 2000;
        }
        if (stripos($job['description'], 'React-Native')) {
            $score = $score + 2000;
        }
         if (stripos($job['description'], 'hybrid app')) {
            $score = $score + 2000;
        }
         if (stripos($job['description'], 'cordova')) {
            $score = $score + 2000;
        }

        return $score;
    }
}