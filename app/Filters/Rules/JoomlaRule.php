<?php namespace App\Filters\Rules;

class JoomlaRule {

    const CATEGORY = 'joomla';

    public static function score ($job) {

        if(stripos($job->title, 'joomla')) {
            return 10000;
        }
        
        if(stripos($job->description, 'joomla')) {
            return 10000;
        }
        
        return -10;
    }
}