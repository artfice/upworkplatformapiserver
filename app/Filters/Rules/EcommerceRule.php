<?php namespace App\Filters\Rules;

class EcommerceRule {

    const CATEGORY = 'ecommerce';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'store')) {
            return 10000;
        }

        if(stripos($job->title, 'shop')) {
            return 10000;
        }


        if(stripos($job->title, 'ecommerce')) {
            return 10000;
        }        

        if(stripos($job->title, 'e-commerce')) {
            return 10000;
        }

        if(stripos($job->title, 'marketplace')) {
            return 10000;
        }        

        if(stripos($job->title, 'woocommerce')) {
            return 10000;
        }

        if(stripos($job->description, 'ecommerce')) {
            $score = $score + 2000;
        }        

        if(stripos($job->description, 'e-commerce')) {
            $score = $score + 2000;
        }

        if(stripos($job->description, 'marketplace')) {
            $score = $score + 2000;
        }        

        if(stripos($job->description, 'woocommerce')) {
            $score = $score + 2000;
        } 

        if(stripos($job->description, 'bug')) {
            $score = -10;
        }        

        if(stripos($job->description, 'fix')) {
            $score = -10;
        }        
        return $score;
    }
}