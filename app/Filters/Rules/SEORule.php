<?php namespace App\Filters\Rules;

class SeoRule {

    const CATEGORY = 'seo';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'SEO Expert')) {
            return 10000;
        }
        if(stripos($job->title, 'seo')) {
            $score = $score + 5000;
        }

        if(stripos($job->description, 'seo')) {
            $score = $score + 1000;
        }        

        if(stripos($job->title, 'SEO Expert')) {
            $score = $score + 1000;
        }
        if(stripos($job->description, 'wordpress') ||
           stripos($job->description, 'ecommerce')) {
            $score = -10;
        }        

        return $score;
    }
}