<?php namespace App\Filters\Rules;

class AssistantRule {

    const CATEGORY = 'assistant';

    public static function score ($job) {
        $score = -10;

if(stripos($job->title, 'Daily Work')) {
    return 20000;
}
if(stripos($job->title, 'Personal Assistant')) {
    return 20000;
}

if(stripos($job->title, 'Tech Support')) {
    return 20000;
}
if(stripos($job->title, 'interview')) {
    return 20000;
}

if(stripos($job->title, ' va ')) {
    return 20000;
}
if(stripos($job->title, 'Data Entry Clerk')) {
    return 20000;
}
if(stripos($job->title, 'phone/email support')) {
    return 20000;
}
if(stripos($job->title, 'phone support')) {
    return 20000;
}
if(stripos($job->title, 'email support')) {
    return 20000;
}
if(stripos($job->title, 'VA Assistant ')) {
    return 20000;
}
if(stripos($job->title, 'Virtual Assistant ')) {
    return 20000;
}
if(stripos($job->title, 'Data Entry')) {
    return 20000;
}
if(stripos($job->title, 'Copy Paste')) {
    return 20000;
}
if(stripos($job->title, 'Data Entry Personel')) {
    return 20000;
}
if(stripos($job->title, 'Wordpress Worker')) {
    return 20000;
}
 
if(stripos($job->title, 'Administrative Assistant/Receptionist')) {
    return 20000;
}
if(stripos($job->title, 'Administrative Receptionist')) {
    return 20000;
}
if(stripos($job->title, 'Receptionist')) {
    return 20000;
}
if(stripos($job->title, 'Expert Virtual Assistant')) {
    return 20000;
}
if(stripos($job->title, 'administrative support')) {
    return 20000;
}
if(stripos($job->title, 'research')) {
    return 20000;
}
if(stripos($job->title, 'Bookkeeper needed')) {
    return 20000;
}
if(stripos($job->title, 'Bookkeeper')) {
    return 20000;
}
if(stripos($job->title, 'Book keeper')) {
    return 20000;
}
if(stripos($job->title, 'Part-Time also available')) {
    return 20000;
}
if(stripos($job->title, 'Full Time Virtual Assistant')) {
    return 20000;
}
if(stripos($job->title, 'part time worker')) {
    return 20000;
}
if(stripos($job->title, 'Representative')) {
    return 20000;
}
if(stripos($job->title, 'Headhunter or Human Resource Executive')) {
    return 20000;
}
if(stripos($job->title, 'Human Resource Executive')) {
    return 20000;
}
if(stripos($job->title, 'Assistant Hiring')) {
    return 20000;
}
if(stripos($job->title, 'Internet based research')) {
    return 20000;
}

if(stripos($job->description, 'during work time')) {
    return 20000;
}
if(stripos($job->description, 'interview')) {
    return 20000;
}
if(stripos($job->description, 'hours / day') ||
stripos($job->description, 'hour/day') ||
stripos($job->description, 'hour / day') ||
stripos($job->description, 'hours/day')) {
    return 20000;
}
if(stripos($job->description, 'Copy Paste')) {
    return 20000;
}
if(stripos($job->description, 'Administrative/Accounting support')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Personal Assistant')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Data Entry Clerk')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'phone/email support')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'phone support')) {
    $score = $score + 20000;
}
if(stripos($job->description, 'email support')) {
    $score = $score + 20000;
}
if(stripos($job->description, 'VA Assistant ')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Virtual Assistant ')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Data Entry')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Data Entry Personel')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Administrative Assistant/Receptionist')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Administrative Receptionist')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Receptionist')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Expert Virtual Assistant')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'administrative support')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'research')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'Bookkeeper needed')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Bookkeeper')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Book keeper')) {
    $score = $score + 10000;
}
if(stripos($job->description, 'Part-Time also available')) {
    $score = $score + 50000;
}
if(stripos($job->description, 'Full Time Virtual Assistant')) {
    $score = $score + 50000;
}
if(stripos($job->description, 'part time worker')) {
    $score = $score + 50000;
}
if(stripos($job->description, 'Representative')) {
    $score = $score + 20000;
}
if(stripos($job->description, 'Headhunter or Human Resource Executive')) {
    $score = $score + 50000;
}
if(stripos($job->description, 'Human Resource Executive')) {
    $score = $score + 50000;
}
if(stripos($job->description, 'Assistant Hiring')) {
    $score = $score + 50000;
}
if(stripos($job->description, ' va ')) {
    $score = $score + 50000;
}
if(stripos($job->description, 'Internet based research')) {
    $score = $score + 20000;
}
if(stripos($job->description, 'Assistant')) {
    $score = $score + 20000;
}
return $score;
}

}