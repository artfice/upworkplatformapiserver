<?php namespace App\Filters\Rules;

class MagentoRule {

    const CATEGORY = 'magento';

    public static function score ($job) {
        $score = -10;
        if(stripos($job->title, 'magento')) {
            return 10000;
        }

        if(stripos($job->title, 'bug')) {
            return -10;
        }        
        if(stripos($job->description, 'bug')) {
            return -10;
        }        
        
        if(stripos($job->description, 'magento')) {
            $score = 5000;
        }
        
        return $score;
    }
}