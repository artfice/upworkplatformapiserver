<?php namespace App\Filters\Rules;

class EmailMarketingRule {

    const CATEGORY = 'email_marketing';

    public static function score ($job) {
        $score = -10;



if(stripos($job->title, 'Email Marketing Specialist')) {
    return 10000;
}
if(stripos($job->title, 'Email Marketing')) {
    return 10000;
}
if(stripos($job->title, 'Email list')) {
    return 10000;
}
if(stripos($job->title, 'aweber')) {
    return 10000;
}
if(stripos($job->title, 'infusionsoft')) {
    return 10000;
}

if(stripos($job->description, 'Email Marketing Specialist')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'Email Marketing')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'Email list')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'aweber')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'infusionsoft')) {
    $score = $score + 1000;
}

return $score;
}

}