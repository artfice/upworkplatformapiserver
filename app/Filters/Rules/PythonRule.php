<?php namespace App\Filters\Rules;

class PythonRule {

    const CATEGORY = 'python';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'python') && !stripos($job->title, 'bug')
        && !stripos($job->title, 'issue') && !stripos($job->title, 'fix')) {
            return 10000;
        }

        if(stripos($job->title, 'Python/Django')) {
            return 10000;
        }        
        if(stripos($job->title, 'Django')) {
            return 10000;
        }        


        if(stripos($job->description, 'python')) {
            $score = $score + 3000;
        }

        if(stripos($job->description, 'django')) {
            $score = $score + 3000;
        } 

        return $score;
    }
}