<?php namespace App\Filters\Rules;

class RubyRule {

    const CATEGORY = 'ruby';

    public static function score ($job) {
        $score = -10;
        
        if(stripos($job->title, 'ruby')) {
            return 10000;
        }

        if(stripos($job->description, 'bug')) {
            return -10;
        }        
        
        if(stripos($job->description, 'ruby')) {
            return 5000;
        }  

        return $score;
    }
}