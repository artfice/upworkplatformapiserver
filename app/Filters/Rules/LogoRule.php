<?php namespace App\Filters\Rules;

class LogoRule {

    const CATEGORY = 'logo';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'logo')) {
            return 10000;
        }

        if(stripos($job->title, 'logo design')) {
            return 10001;
        }

        if(stripos($job->description, 'logo')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'design')) {
            $score = $score + 1000;
        }        

       if(stripos($job->description, 'brand')) {
            $score = $score + 1000;
        } 

        if(stripos($job->description, 'wordpress')) {
            $score = -10;
        } 
        if(stripos($job->description, 'ecommerce')) {
            $score = -10;
        }         
       return $score;
    }
}