<?php namespace App\Filters\Rules;

class PhpRule {

    const CATEGORY = 'php';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'php') && !stripos($job->title, 'bug')
        && !stripos($job->title, 'fix') && !stripos($job->title, 'issue')) {
            return 10000;
        }

        if(stripos($job->description, 'php')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'wordpress')) {
            $score = -10;
        }        

        if(stripos($job->description, 'portal')) {
            $score = $score + 1000;
        }  

        if(stripos($job->description, 'bug')) {
            $score = -10;
        }        

        if(stripos($job->description, 'fix')) {
            $score = -10;
        }        

        return $score;
    }
}