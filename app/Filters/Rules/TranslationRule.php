<?php namespace App\Filters\Rules;

class TranslationRule {

    const CATEGORY = 'translation';

    public static function score ($job) {
        $score = -10;

        if (stripos($job['title'], 'translation')) {
            return 10000;
        }
         if (stripos($job['title'], 'translate')) {
            return 10000;
        }
         if (stripos($job['title'], 'translator')) {
            return 10000;
        }
        if (stripos($job['description'], 'translation')) {
            $score = $score + 2000;
        }
        
         if (stripos($job['description'], 'translate')) {
            $score = $score + 2000;
        }
         if (stripos($job['description'], 'translator')) {
            $score = $score + 4000;
        }
         if (stripos($job['description'], 'into chinese')) {
            $score = $score + 2000;
        }

        return $score;
    }
}