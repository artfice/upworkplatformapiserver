<?php namespace App\Filters\Rules;

class TeamRule {

    const CATEGORY = 'team';

    public static function score ($job) {
        $score = -10;
        if (stripos($job->description, 'full time')) {
            return 10000;
        }  
        if (stripos($job->description, 'part time')) {
            return 10000;
        }  
        if (stripos($job->title, 'team')) {
            return 10000;
        }
        if (stripos($job->title, 'for a senior')) {
            return 10000;
        }
        if (stripos($job->title, 'Long Term Work Potential') ) {
            return 10000;
        }        
        if (stripos($job->title, 'Work Potential') ) {
            return 10000;
        }
        if (stripos($job->title, 'benefits and paid vacation') ) {
            return 10000;
        }
        if (stripos($job->title, 'paid vacation') ) {
            return 10000;
        }
        if (stripos($job->title, 'Resume and CV update') ) {
            return 10000;
        }
        if (stripos($job->title, 'CV update') ) {
            return 10000;
        }
        if (stripos($job->title, 'commission') ) {
            return 10000;
        }
        if (stripos($job->title, 'Resume') ) {
            return 10000;
        }
        if (stripos($job->title, 'various positions') ) {
            return 10000;
        }
        if (stripos($job->title, 'Sales Lead') ) {
            return 10000;
        }
        if (stripos($job->title, 'Marketing Lead') ) {
            return 10000;
        }
        if (stripos($job->description, 'join')) {
            $score = $score + 500;
        }    
        if (stripos($job->description, 'join our team')) {
            $score = $score + 8000;
        }        
        if (stripos($job->description, 'web design company')) {
            $score = $score + 8000;
        }        
        if (stripos($job->description, 'developer')) {
            $score = $score + 1000;
        }        

        if (stripos($job->description, 'engineer')) {
            $score = $score + 2000;
        }        

        if (stripos($job->description, 'front end developer')) {
            $score = $score + 1000;
        }        

        if (stripos($job->description, 'frontend developer')) {
            $score = $score + 1000;
        }        

        if (stripos($job->description, 'software engineer')) {
            $score = $score + 2000;
        }        

        if (stripos($job->description, 'designer')) {
            $score = $score + 1000;
        }        

        if (stripos($job->description, 'agency')) {
            $score = $score + 2000;
        }        

        if (stripos($job->description, 'fulltime')) {
            $score = $score + 2000;
        }    
        if (stripos($job->description, 'full time')) {
            $score = $score + 2000;
        }  
        if (stripos($job->description, 'part time')) {
            $score = $score + 2000;
        }  
        if (stripos($job->description, 'parttime')) {
            $score = $score + 2000;
        }
        if (stripos($job->description, 'looking for a senior')) {
            $score = $score + 2000;
        }

        if (stripos($job->description, 'Long Term Work Potential')){
            $score = $score + 2000;
        }
        if (stripos($job->description, 'benefits and paid vacation')){
            $score = $score + 2000;
        }
        if (stripos($job->description, 'Resume and CV update')){
            $score = $score + 2000;
        }
        if (stripos($job->description, 'various positions')){
            $score = $score + 2000;
        }
        if (stripos($job->description, 'Sales Lead')){
            $score = $score + 2000;
        }
        if (stripos($job->description, 'Marketing Lead')){
            $score = $score + 2000;
        }

        return $score;
    }
}