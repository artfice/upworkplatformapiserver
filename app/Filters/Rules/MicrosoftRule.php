<?php namespace App\Filters\Rules;

class MicrosoftRule {

    const CATEGORY = 'microsoft';

    public static function score ($job) {
        $score = -10;
 
if(stripos($job->title, 'POINT-POINT')) {
    return 10000;
}

if(stripos($job->title, 'Excel VBA')) {
    return 10000;
}
if(stripos($job->title, 'PowerPoint slide')) {
    return 10000;
}
if(stripos($job->title, 'excel spreadsheet')) {
    return 10000;
}
if(stripos($job->title, 'advanced excel users only')) {
    return 10000;
}
if(stripos($job->title, 'Microsoft Windows')) {
    return 10000;
}
if(stripos($job->title, 'Win10')) {
    return 10000;
}
if(stripos($job->title, 'Microsoft Access')) {
    return 10000;
}
if(stripos($job->title, 'Microsoft Office')) {
    return 10000;
}
if(stripos($job->title, 'Microsoft Doc')) {
    return 10000;
}
if(stripos($job->title, 'Microsoft Excel')) {
    return 10000;
}
if(stripos($job->title, 'Excel')) {
    return 10000;
}
if(stripos($job->title, 'VBA')) {
    return 10000;
}
if(stripos($job->title, 'spreadsheet')) {
    return 10000;
}
if(stripos($job->title, 'PowerPoint')) {
    return 10000;
}
if(stripos($job->title, 'keynote')) {
    return 10000;
}

if(stripos($job->description, 'Excel VBA')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'PowerPoint slide')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'excel spreadsheet')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'advanced excel users only')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'Microsoft Windows')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'Win10')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'Microsoft Access')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'Microsoft Office')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'Microsoft Doc')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'Microsoft Excel')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'Excel')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'VBA')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'spreadsheet')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'PowerPoint')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'keynote')) {
    $score = $score + 2000;
}

        
        return $score;
    }
}