<?php namespace App\Filters\Rules;

class GraphicDesignRule {

    const CATEGORY = 'graphic_design';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'graphic')) {
            $score = $score + 2000;
        }
        if(stripos($job->title, 'Re-touch')) {
            return 10000;
        }
        if(stripos($job->title, 'photo edit')) {
            return 10000;
        }

        if(stripos($job->title, '2D Paintings')) {
            return 10000;
        }

        if(stripos($job->title, 'Design Expert')) {
            return 10000;
        }

        if(stripos($job->title, 'design')) {
            $score = $score + 2000;
        }

        if(stripos($job->title, 'graphic design')) {
            return 10000;
        }

        if(stripos($job->title, 'brand')) {
            $score = $score + 2000;
        }

        if(stripos($job->description, 'graphic')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'graphic design')) {
            $score = $score + 5000;
        }
        if(stripos($job->description, 'drawings')) {
            $score = $score + 4000;
        }

        if(stripos($job->description, 'design')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'brand')) {
            $score = $score + 1000;
        }        
        if(stripos($job->description, 'infographic')) {
            $score = $score + 3000;
        }        
        if(stripos($job->description, 't-shirt')) {
            $score = $score + 3000;
        }        
        if(stripos($job->description, 't shirt')) {
            $score = $score + 3000;
        }        

        if(stripos($job->description, 'advertise')) {
            $score = -10;
        }    

        if(stripos($job->description, 'website')) {
            $score = -10;
        }    

        if(stripos($job->description, 'web site')) {
            $score = -10;
        }    

        if(stripos($job->description, 'web-site')) {
            $score = -10;
        }    

        if(stripos($job->description, 'wordpress')) {
            $score = -10;
        }    

        if(stripos($job->description, 'ecommerce')) {
            $score = -10;
        }    

        if(stripos($job->description, 'translation')) {
            $score = -10;
        }    
        return $score;
    }
}