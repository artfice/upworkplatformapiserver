<?php namespace App\Filters\Rules;

class IOSRule {

    const CATEGORY = 'ios';

    public static function score ($job) {
        $score = -10;
        if (stripos($job['title'], 'ios')) {
            return 10000;
        }

        if (stripos($job['title'], 'swift')) {
            return 10000;
        }

        if (stripos($job['title'], 'objectivec')) {
            return 10000;
        }

        if (stripos($job['title'], 'apple')) {
            $score = $score + 2000;
        }

        if (stripos($job['title'], 'app')) {
            $score = $score + 1000;
        }


        if (stripos($job['description'], 'ios')) {
            $score = $score + 1000;
        }
        if (stripos($job['description'], 'objectivec')) {
            $score = $score + 3000;
        }


        if (stripos($job['description'], 'swift')) {
            $score = $score + 1000;
        }


        if (stripos($job['description'], 'apple')) {
            $score = $score + 1000;
        }

        if (stripos($job['description'], 'app')) {
            $score = $score + 1000;
        }
        return $score;
    }
}