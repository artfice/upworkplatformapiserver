<?php namespace App\Filters\Rules;

class SquareSpaceRule {

    const CATEGORY = 'square_space';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'squarespace')) {
            return 10000;
        }
        if(stripos($job->title, 'wix')) {
            return 10000;
        }
        if(stripos($job->title, 'weebly')) {
            return 10000;
        }
        if(stripos($job->description, 'squarespace')) {
            return 5000;
        }
        if(stripos($job->description, 'wix')) {
            return 5000;
        }
        if(stripos($job->description, 'weebly')) {
            return 5000;
        }        

        return $score;
    }
}