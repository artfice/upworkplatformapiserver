<?php namespace App\Filters\Rules;

class LawyerRule {

    const CATEGORY = 'lawyer';

    public static function score ($job) {
        $score = -10;

if(stripos($job->title, 'Legal Legal advice needed')) {
    return 10000;
}
if(stripos($job->title, 'lawyers')) {
    return 10000;
}
if(stripos($job->title, 'employment contract law')) {
    return 10000;
}
if(stripos($job->title, 'lobbyist')) {
    return 10000;
}
if(stripos($job->title, ' lawyer')) {
    return 10000;
}

if(stripos($job->description, 'Legal Legal advice needed')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'lawyers')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'employment contract law')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'lobbyist')) {
    $score = $score + 1000;
}
if(stripos($job->description, ' lawyer')) {
    $score = $score + 1000;
}

return $score;
}

}