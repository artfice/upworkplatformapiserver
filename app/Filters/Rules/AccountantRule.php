<?php namespace App\Filters\Rules;

class AccountantRule {

    const CATEGORY = 'accountant';

    public static function score ($job) {
        $score = -10;



if(stripos($job->title, 'Accountant')) {
    return 10000;
}
if(stripos($job->title, 'bookkeeper')) {
    return 10000;
}
if(stripos($job->title, 'accounting')) {
    return 10000;
}
if(stripos($job->title, 'Tax help')) {
    return 10000;
}
if(stripos($job->title, ' IRA')) {
    return 10000;
}

if(stripos($job->description, 'Accountant')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'bookkeeper')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'accounting')) {
    $score = $score + 1000;
}
if(stripos($job->description, 'Tax help')) {
    $score = $score + 1000;
}
if(stripos($job->description, ' IRA')) {
    $score = $score + 1000;
}

return $score;
}

}