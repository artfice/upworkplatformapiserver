<?php namespace App\Filters\Rules;

class IndiaRule {

    const CATEGORY = 'india';

    public static function score ($job) {

        if (stripos($job->title, 'india')) {
            return 20000;
        }
        if (stripos($job->title, 'pakistan')) {
            return 20000;
        }
        if (stripos($job->description, 'india')) {
            return 20000;
        }
        if (stripos($job->title, 'pakistan')) {
            return 20000;
        }
        if (stripos($job->title, 'Ukraine ONLY')) {
            return 20000;
        }

          if (stripos($job->description, 'do not apply')) {
            return 20000;
        }
          if (stripos($job->description, 'Singapore')) {
            return 20000;
        }
          if (stripos($job->description, 'not-for-profit') || 
              stripos($job->description, 'not for profit') || 
              stripos($job->description, 'nonprofit') ||
              stripos($job->description, 'nonprofit')) {
            return 20000;
        }
          if (stripos($job->description, 'is a MUST')) {
            return 20000;
        }
          if (stripos($job->description, 'scam')) {
            return 20000;
        }
          if (stripos($job->description, 'Client site')) {
            return 20000;
        }
          if (stripos($job->description, 'i have a client')) {
            return 20000;
        }

        return -100;
    }
}