<?php namespace App\Filters\Rules;

class ASPRule {

    const CATEGORY = 'asp';

    public static function score ($job) {
        $score = -10;
        if (stripos($job->title, ' asp')) {
            return 10000;
        }

        if (stripos($job->title, 'c#')) {
            return 10000;
        }

        if (stripos($job->title, '.net')) {
            return 10000;
        }

        if (stripos($job->description, 'c#')) {
            $score = $score + 1000;
        }

        if (stripos($job->description, ' asp')) {
            $score = $score + 1000;
        }
        if (stripos($job->description, '.net')) {
            $score = $score + 1000;
        }   
        return $score;
    }
}