<?php namespace App\Filters\Rules;

class VideoRule {

    const CATEGORY = 'video_edit';

    public static function score ($job) {
        $score = -10;
if(stripos($job->title, 'Youtube Channel')) {
    return 10000;
}
if(stripos($job->title, 'Introduction Video')) {
    return 10000;
} 
if(stripos($job->title, 'Sony Vegas Pro')) {
    return 10000;
}
if(stripos($job->title, 'video editing')) {
    return 10000;
}
if(stripos($job->title, 'video edit')) {
    return 10000;
}

if(stripos($job->title, 'Second Video')) {
    return 10000;
}
if(stripos($job->title, 'TV Channel')) {
    return 10000;
}
if(stripos($job->title, 'video promo editor')) {
    return 10000;
}
if(stripos($job->title, 'edit video')) {
    return 10000;
}
if(stripos($job->title, 'explainer video')) {
    return 10000;
}
if(stripos($job->title, 'YouTube Video Editor')) {
    return 10000;
}
if(stripos($job->title, 'video production expert')) {
    return 10000;
}        
if(stripos($job->title, 'video production')) {
    return 10000;
}

if(stripos($job->description, 'Youtube Channel')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'documentary')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'Introduction Video')) {
    $score = $score + 2000;
} 
if(stripos($job->description, 'Sony Vegas Pro')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'video editing')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'video edit')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'Second Video')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'TV Channel')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'video promo editor')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'edit video')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'explainer video')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'YouTube Video Editor')) {
    $score = $score + 2000;
}
if(stripos($job->description, 'video production expert')) {
    $score = $score + 2000;
}        
if(stripos($job->description, 'video production')) {
    $score = $score + 2000;
}        
        return $score;
    }
}