<?php namespace App\Filters\Rules;

class WordpressRule {

    const CATEGORY = 'wordpress';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'wordpress')) {
            return 9999;
        }

        if(stripos($job->title, 'divi')) {
            return 10000;
        }

        if(stripos($job->title, 'avada')) {
            return 10000;
        }

        if(stripos($job->description, 'wordpress')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'theme')) {
            $score = $score + 1000;
        } 

        if(stripos($job->title, 'divi')) {
            $score = $score + 1000;
        }

        if(stripos($job->title, 'avada')) {
            $score = $score + 1000;
        }

        if(stripos($job->description, 'ecommerce')) {
            $score = -10;
        }        

        if(stripos($job->description, 'woocommerce')) {
            $score = -10;
        }       

        if(stripos($job->description, 'bug')) {
            $score = -10;
        }        

        if(stripos($job->description, 'fix')) {
            $score = -10;
        }        

        return $score;
    }
}