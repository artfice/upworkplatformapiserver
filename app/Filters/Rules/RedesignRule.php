<?php namespace App\Filters\Rules;

class RedesignRule {

    const CATEGORY = 'redesign';

    public static function score ($job) {
        $score = -10;
        
        if(stripos($job->title, 'redesign')) {
            $score = $score + 2000;
        }

        if(stripos($job->title, 'ux')) {
             $score = $score + 2000;
        }
        if(stripos($job->title, 'ui')) {
            $score = $score + 2000;
        }        

        if(stripos($job->description, 'responsive')) {
            $score = $score + 1000;
        } 
        if(stripos($job->description, 'mobile responsive')) {
            $score++;
        }
         if(stripos($job->description, 'redesign')) {
            $score = $score + 1000;
        } 
          if(stripos($job->description, 'ux')) {
            $score = $score + 1000;
        } 
          if(stripos($job->description, 'ui')) {
            $score = $score + 1000;
        } 

        if(stripos($job['description'], 'wordpress') ||
           stripos($job['description'], 'ecommerce')) {
            $score = -10;
        }        

        return $score;
    }
}