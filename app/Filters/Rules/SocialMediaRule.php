<?php namespace App\Filters\Rules;

class SocialMediaRule {

    const CATEGORY = 'social_media';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'social')) {
            $score = $score + 2000;
        }

        if(stripos($job->title, 'Social media management')) {
            return 10000;
        }

        if(stripos($job->description, 'facebook')) {
            $score = $score + 2000;
        }        

        if(stripos($job->description, 'instagram')) {
            $score = $score + 2000;
        }        

        if(stripos($job->description, 'pinterest')) {
            $score = $score + 2000;
        }        

        if(stripos($job->title, 'Social media')) {
            $score = $score + 3000;
        }

        if(stripos($job->description, 'media')) {
            $score = $score + 500;
        }        

        if(stripos($job->description, 'Social media management')) {
            $score = $score + 5000;
        }


        if(stripos($job->description, 'facebook')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'instagram')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'pinterest')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'manage')) {
            $score = $score + 500;
        }  

        if(stripos($job->description, 'already')) {
            $score = $score + 500;
        }  

        if(stripos($job->description, 'wordpress') ||
           stripos($job->description, 'ecommerce')) {
            $score = -10;
        }        

        return $score;
    }
}