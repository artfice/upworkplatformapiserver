<?php namespace App\Filters\Rules;

class MarketingRule {

    const CATEGORY = 'marketing';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->description, 'marketing')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'landing')) {
            $score = $score + 1000;
        }        

         if(stripos($job->description, 'traffic')) {
            $score = $score + 1000;
        }  
         if(stripos($job->description, 'drive')) {
            $score = $score + 1000;
        }  
         if(stripos($job->description, 'exist')) {
            $score = $score + 1000;
        }  

        if(stripos($job->description, 'existing')) {
            $score = $score + 1000;
        }  

        if(stripos($job->description, 'facebook')) {
            $score = $score - 1000;
        }  

        if(stripos($job->description, 'instagram')) {
            $score = $score - 1000;
        }  

        if(stripos($job->description, 'pinterest')) {
            $score = $score - 1000;
        }  

        if(stripos($job->description, 'wordpress') ||
           stripos($job->description, 'ecommerce')) {
            $score = -10;
        }        

        return $score;
    }
}