<?php namespace App\Filters\Rules;

class AndroidRule {

    const CATEGORY = 'android';

    public static function score ($job) {
        $score = -10;

        if (stripos($job['title'], 'android')) {
            return 10000;
        }

        if (stripos($job['title'], 'ios')) {
            return -10000;
        }

        if (stripos($job['title'], 'hybrid app')) {
            return -10000;
        }

        if (stripos($job['title'], 'android developer')) {
            return 10000;
        }

        if (stripos($job['description'], 'android')) {
            return 5000;
        }
        if (stripos($job['description'], 'mobile app developers')) {
            return 5000;
        }
        if (stripos($job['description'], 'mobile app agency')) {
            return 5000;
        }

        if (stripos($job['description'], 'marshmallow')) {
            $score = $score + 2000;
        }

        if (stripos($job['description'], 'java')) {
            $score = $score + 1000;
        }



        if (stripos($job['description'], 'app')) {
            $score = $score + 1000;
        }
        return $score;
    }
}