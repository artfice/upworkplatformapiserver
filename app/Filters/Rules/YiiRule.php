<?php namespace App\Filters\Rules;

class YiiRule {

    const CATEGORY = 'yii';

    public static function score ($job) {

        if(stripos($job->description, 'yii')) {
            return 10000;
        }        

        if(stripos($job->title, 'yii')) {
            return 10000;
        }
        
        return -10;
    }
}