<?php namespace App\Filters\Rules;

class FacebookAdRule {

    const CATEGORY = 'facebook_ads';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'marketing')) {
            $score = $score + 1000;
        }        

        if(stripos($job->title, 'facebook ads')) {
            return 10000;
        }

        if(stripos($job->title, 'facebookads')) {
            return 10000;
        }        

        if(stripos($job->title, 'facebook-ads')) {
            return 10000;
        }        

        if(stripos($job->title, 'facebook ad')) {
            return 10000;
        }        

        if(stripos($job->title, 'facebook-ad')) {
            return 10000;
        }

        if(stripos($job->title, 'FB page')) {
            return 10000;
        }

        if(stripos($job->description, 'marketing')) {
            $score = $score + 1000;
        }    
    
        if(stripos($job->description, 'facebook')) {
            $score = $score + 1000;
        }  

        if(stripos($job->description, 'facebook ad')) {
            $score = $score + 1000;
        }  

        if(stripos($job->description, 'facebookad')) {
            $score = $score + 1000;
        }  
    
        if(stripos($job->description, 'ad')) {
            $score = $score + 1000;
        }  

        if(stripos($job->description, 'campaign')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'wordpress') ||
           stripos($job->description, 'ecommerce')) {
            $score = $score - 2000;
        }        

        return $score;
    }
}