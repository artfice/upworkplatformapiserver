<?php namespace App\Filters\Rules;

class LandingRule {

    const CATEGORY = 'landing';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'landing page')) {
            return 10000;
        }

        if(stripos($job->title, 'landing')) {
            return 10000;
        }

        if(stripos($job->title, 'clickfunnel')) {
            return 10000;
        }        

        if(stripos($job->title, 'instapage')) {
            return 10000;
        }        

        if(stripos($job->title, 'leadpage')) {
            return 10000;
        }     
        if(stripos($job->title, 'PPC')) {
            return 10000;
        }     

        if(stripos($job->description, 'landing')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'clickfunnel')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'instapage')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, 'leadpage')) {
            $score = $score + 1000;
        }        
        if(stripos($job->description, 'infusionsoft')) {
            $score = $score + 1000;
        }    
        if(stripos($job->description, 'infusion soft')) {
            $score = $score + 1000;
        }    
        if(stripos($job->description, 'optimizepress')) {
            $score = $score + 1000;
        }    
        if(stripos($job->description, 'optimize press')) {
            $score = $score + 1000;
        }    
        if(stripos($job->description, 'ppc')) {
            $score = $score + 1000;
        }    

        if(stripos($job->description, 'wordpress') ||
           stripos($job->description, 'ecommerce')) {
            $score = -10;
        }        
    

        return $score;
    }
}