<?php namespace App\Filters\Rules;

class FrontendRule {

    const CATEGORY = 'html';

    public static function score ($job) {
        $score = -10;
        
        if(stripos($job->title, 'convert')) {
            return 10000;
        }
        if(stripos($job->title, 'PSD to HTML')) {
            return 10000;
        }

        if(stripos($job->title, ' PSD')) {
            return 10000;
        }
        if(stripos($job->title, 'Angular2')) {
            return 10000;
        } 

        if(stripos($job->title, 'Angular')) {
            return 10000;
        }         

        if(stripos($job->title, 'React Native') ||
           stripos($job->title, 'ReactNative') ||
           stripos($job->title, 'React-Native')) {
            return -10;
        }   

        if(stripos($job->title, 'React')) {
            return 10000;
        }
        if(stripos($job->title, 'Backbone')) {
            return 10000;
        }
        if(stripos($job->title, 'Jquery')) {
            return 10000;
        } 

        if(stripos($job->title, 'responsive')) {
            return 10000;
        } 

        if(stripos($job->description, 'responsive')) {
            $score = $score + 500;
        } 

        if(stripos($job->description, 'html')) {
            $score = $score + 1000;
        } 

        if(stripos($job->description, 'css')) {
            $score = $score + 1000;
        } 

        if(stripos($job->description, 'javascript')) {
            $score = $score + 1000;
        } 

        if(stripos($job->description, 'angular')) {
            $score = $score + 1000;
        } 
        if(stripos($job->description, 'Angular2')) {
            $score = $score + 1000;
        } 
    
        if(stripos($job->description, 'react')) {
            $score = $score + 1000;
        } 

        if(stripos($job->description, 'backbone')) {
            $score = $score + 1000;
        } 

        if(stripos($job->description, 'jquery')) {
            $score = $score + 1000;
        } 

        if(stripos($job->description, 'convert')) {
            $score = $score + 1000;
        }        

        if(stripos($job->description, ' psd')) {
            $score = $score + 1000;
        }        

        if(stripos($job['description'], 'wordpress') ||
           stripos($job['description'], 'ecommerce')) {
            $score = -10;
        }        
        
        return $score;
    }
}