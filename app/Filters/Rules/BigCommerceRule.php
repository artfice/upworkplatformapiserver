<?php namespace App\Filters\Rules;

class BigCommerceRule {

    const CATEGORY = 'bigcommerce';

    public static function score ($job) {
        $score = -10;
        if(stripos($job->title, 'bigcommerce')) {
            return 10000;
        }

        if(stripos($job->title, 'big commerce')) {
            return 10000;
        }

        if(stripos($job->title, 'big-commerce')) {
            return 10000;
        }

        if(stripos($job->description, 'bigcommerce')) {
            $score = $score + 5000;
        }        


        if(stripos($job->description, 'big commerce')) {
            $score = $score + 5000;
        }        


        if(stripos($job->description, 'big-commerce')) {
            $score = $score + 2000;
        }        


       
        
        return $score;
    }
}