<?php namespace App\Filters\Rules;

class OpenCartRule {

    const CATEGORY = 'opencart';

    public static function score ($job) {
        $score = -10;
        if(stripos($job->title, 'opencart')) {
            return 1000;
        }
        if(stripos($job->title, 'open cart')) {
            return 1000;
        }
        if(stripos($job->title, 'open-cart')) {
            return 1000;
        }

        if(stripos($job->description, 'opencart')) {
            $score = $score + 4000;
        }        


        
        if(stripos($job->description, 'open cart')) {
            $score = $score + 4000;
        }        


        return $score;
    }
}