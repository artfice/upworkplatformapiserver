<?php namespace App\Filters\Rules;

class ShopifyRule {

    const CATEGORY = 'shopify';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'shopify')) {
            return 10000;
        }

        if(stripos($job->description, 'shopify')) {
            return 5000;
        }

        return $score;

    }
}