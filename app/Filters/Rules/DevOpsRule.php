<?php namespace App\Filters\Rules;

class DevOpsRule {

    const CATEGORY = 'devops';

    public static function score ($job) {
        $score = -10;

if(stripos($job->title, 'linux')) {
    return 10000;
}
if(stripos($job->title, 'unix')) {
    return 10000;
}
if(stripos($job->title, 'widnows IIS')) {
    return 10000;
}
if(stripos($job->title, 'aws server')) {
    return 10000;
}
if(stripos($job->title, ' elasticbeanstalk')) {
    return 10000;
}
if(stripos($job->title, ' cloudformation')) {
    return 10000;
}

if(stripos($job->description, 'linux')) {
    $score = $score + 1000;
}

if(stripos($job->description, 'linux driver')) {
    $score = $score + 4000;
}
if(stripos($job->description, 'file systems')) {
    $score = $score + 4000;
}
if(stripos($job->description, 'ddos')) {
    $score = $score + 10000;
}

if(stripos($job->description, 'unix')) {
    $score = $score + 4000;
}
if(stripos($job->description, 'windows IIS')) {
    $score = $score + 4000;
}
if(stripos($job->description, 'aws server')) {
    $score = $score + 2000;
}
if(stripos($job->description, ' elasticbeanstalk')) {
    $score = $score + 3000;
}
if(stripos($job->description, ' cloudformation')) {
    $score = $score + 3000;
}


return $score;
}

}