<?php namespace App\Filters\Rules;

class MembershipRule {

    const CATEGORY = 'membership';

    public static function score ($job) {
        $score = -10;
        if (stripos($job['title'], 'membership')) {
            return 10000;
        }
        if (stripos($job['title'], 'mastermind')) {
            return 10000;
        }

        if (stripos($job['description'], 'membership')) {
            $score = $score + 1000;
        }

         if (stripos($job['description'], 'restrict')) {
            $score = $score + 1000;
        }
         if (stripos($job['description'], 'blog')) {
            $score = $score + 1000;
        }
         if (stripos($job['description'], 'paywall')) {
            $score = $score + 1000;
        }
         if (stripos($job['description'], 'pay wall')) {
            $score = $score + 1000;
        }
        if (stripos($job['description'], 'paid')) {
            $score = $score + 1000;
        }

        return $score;
    }
}