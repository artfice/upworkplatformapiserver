<?php namespace App\Filters\Rules;

class BlogWritingRule {

    const CATEGORY = 'blog_writing';

    public static function score ($job) {
        $score = -10;


if(stripos($job->title, 'stories')) {
    return 10000;
}
if(stripos($job->title, ')words')) {
    return 10000;
}
if(stripos($job->title, 'Academic Writers Needed')) {
    return 10000;
}
if(stripos($job->title, 'Editors')) {
    return 10000;
}
if(stripos($job->title, 'Proofreaders')) {
    return 10000;
}
if(stripos($job->title, 'Copy Editing')) {
    return 10000;
}
if(stripos($job->title, 'Web Site Content')) {
    return 10000;
}
if(stripos($job->title, 'writer needed')) {
    return 10000;
}
if(stripos($job->title, 'Writers')) {
    return 10000;
}
if(stripos($job->title, 'Copywriter')) {
    return 10000;
}
if(stripos($job->title, 'promotional texts')) {
    return 10000;
}
if(stripos($job->title, 'article writer')) {
    return 10000;
}
if(stripos($job->title, 'High Quality Writer Needed')) {
    return 10000;
}
if(stripos($job->title, 'Technology Writing')) {
    return 10000;
}
if(stripos($job->title, 'Scientific Research')) {
    return 10000;
}
if(stripos($job->title, 'Writer')) {
    return 10000;
}
if(stripos($job->title, 'Content Writer')) {
    return 10000;
}
if(stripos($job->title, 'Copywriter')) {
    return 10000;
}
if(stripos($job->title, 'Copywriter')) {
    return 10000;
}
if(stripos($job->title, 'Copy writer')) {
    return 10000;
}
if(stripos($job->title, 'Quality writers')) {
    return 10000;
}
if(stripos($job->title, 'Business Plan Writer')) {
    return 10000;
}
if(stripos($job->title, 'Proofreading')) {
    return 10000;
}

if(stripos($job->title, 'blog')) {
    return 10000;
}

if(stripos($job->title, 'blog writing')) {
    return 10000;
} 
if(stripos($job->title, 'Strong Writers')) {
    return 10000;
} 

if(stripos($job->description, 'stories')) {
    $score = $score + 5000;
}
if(stripos($job->description, ')words')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Academic Writers Needed')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Editors')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Proofreaders')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Copy Editing')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Web Site Content')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'writer needed')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Writers')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Copywriter')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'promotional texts')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'article writer')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'High Quality Writer Needed')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Technology Writing')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Scientific Research')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Writer')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Content Writer')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Copywriter')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Copywriter')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Copy writer')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Quality writers')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Business Plan Writer')) {
    $score = $score + 5000;
}
if(stripos($job->description, 'Proofreading')) {
    $score = $score + 5000;
}

if(stripos($job->description, 'blog')) {
    $score = $score + 5000;
}

if(stripos($job->description, 'blog writing')) {
    $score = $score + 5000;
} 
if(stripos($job->description, 'Strong Writers')) {
    $score = $score + 5000;
} 
        if(stripos($job->description, 'wordpress')) {
            $score = -10;
        }
                
        return $score;
    }
}