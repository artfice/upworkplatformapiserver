<?php namespace App\Filters\Rules;

class GoogleAdWordRule {

    const CATEGORY = 'google_adword';

    public static function score ($job) {
        $score = -10;

        if(stripos($job->title, 'google adword')) {
            return 10000;
        }
        if(stripos($job->title, 'google adwords')) {
            return 10000;
        }

        if(stripos($job->title, 'googleadwords')) {
            return 10000;
        }

        if(stripos($job->title, 'adword')) {
            return 10000;
        }
        if(stripos($job->title, 'ad word')) {
            return 10000;
        }

        if(stripos($job->description, 'adword')) {
            $score = $score + 1000;
        }        
        if(stripos($job->description, 'ad word')) {
            $score = $score + 1000;
        }        
        if(stripos($job->description, 'drive')) {
            $score = $score + 500;
        }        
        if(stripos($job->description, 'search')) {
            $score = $score + 500;
        }        
        if(stripos($job->description, 'visibility')) {
            $score = $score + 500;
        }        
        if(stripos($job->description, 'people')) {
            $score = $score + 500;
        }              

        return $score;
    }
}