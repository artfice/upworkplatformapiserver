<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Upwork Proposal Dashboard</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.indigo-orange.min.css" />
<script defer src="https://code.getmdl.io/1.2.1/material.min.js"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }
            .pull-left {
                float: left;
            }
            .pull-right {
                float: right;
            }
            .clear {
                clear: both;
            }
            .proposal-container {
                width: 48%;
                float:left;
            }
            #proposal {
                width: 100%;
            }
            .proposal-url-container {
                width: 48%;
                float:right;
            }
            #proposal-urls {
                width: 100%;
            }
            .job-table table {
                border-collapse: collapse;
                width: 100%;
            }

            table td {
              padding-bottom: 10px;
              text-align: center;
            }

            table tr {
              border-bottom: 1px solid #e2e2e2;
            }
            #javascript {
                width: 100%;
                height: 200px;
            }
        </style>
    </head>
    <body>
        <div class="pull-left">
            <select id="categories" onchange="window.location.search = 'category=' + this.value;">
                <option value="all">All</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->slug }}" @if($selectedCategory->slug == $category->slug) selected @endif>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="pull-right">
            <button id="process-all" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onclick="window.location.search = 'process=1&category='  + document.getElementById('categories').value;">Process All</button>
        </div>
        <div class="clear"></div>
        <div class="proposal-container">
<br/>
            <h5>Proposal To Send</h5>
<br/>
            <textarea class="mdl-textfield__input" name="proposal" id="proposal" cols="30" rows="40" 
            onclick="this.setSelectionRange(0, 1000000000)">{!! $selectedCategory->proposal !!}</textarea>
        </div>
        <div class="proposal-url-container">
<br/>
            <h5>Proposal URL to Send</h5>
<br/>
            <textarea class="mdl-textfield__input" name="proposal-urls" id="proposal-urls" cols="30" rows="40" 
            onclick="this.setSelectionRange(0, 1000000000)">@foreach ($urls as $key => $url)@if($key % 30 == 0 && $key > 0)&#13;&#10; @endif{{ $url }}&#13;&#10;@endforeach            
            </textarea>
        </div>
        <div class="clear"></div>
        <br/>
        <div class="job-table">
<br/>
            <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                <thead>
                <th>ID</th>
                <th>Title</th>
                <th>Type</th>
                <th>Budget</th>
                <th>Time</th>
                <th>Processed</th>
                <th>Description</th>
                </thead>
                <tbody>
                @foreach ($jobs as $job)
                    <tr>
                        <td>{{$job->id}}<br/>
<a class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" target="__blank" href="https://upwork.manaknightdigital.com/api/upwork/reprocess?id={{$job->id}}">Reprocess</a>
<br/><br/>
<a style="float:left" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" target="__blank" href="https://upwork.manaknightdigital.com/api/upwork/cancel?id={{$job->id}}">Ignore it</a>
                        </td>
                        <td><a  
                        target="__blank" 
                        href="https://www.upwork.com{{ $job->url }}"
                        >{{$job->title}}</a></td>
                        <td>{{$job->type == 'fixed' ? 'F' : 'H'}}</td>
                        <td>{{$job->budget}}</td>
                        <td>{!! str_replace(['+00:00', 'T'], ['', '<br/>'],$job->time) !!}
                        <br/>
                        {{$job->country}}
                        </td>
                        <td>{{$job->processed === 1 ? 'Y' : 'N'}}
                        <br/>
                        <a class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" target="__blank" href="https://upwork.manaknightdigital.com/api/upwork/process/report?id={{$job->id}}">Report</a>
                        </td>
                        <td>
                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onclick="document.getElementById('description-job-{{ $job->id }}').style = '';this.style='display:none';">Show</button>
                            <div id="description-job-{{ $job->id }}" style="display:none">{!! $job->description !!}</div>
                        </td>                    
                    </tr>
                @endforeach
                </tbody>
            </table>
<br/>
        </div>

<div class="javascript-container">
<br/>
            <h5>Javascript Code to paste [All Category]</h5>
<br/>
            <textarea class="mdl-textfield__input" name="javascript" id="javascript" onclick="this.setSelectionRange(0, 1000000000)">
console.log('start upwork script');

var ignoreCountry = ['India', 'Pakistan', 'United Arab Emirates', 'Israel', 
'South Africa', 'Aruba', 'Singapore', 'Nigeria', 'Brazil', 'Ukraine', 'Jordan',
'Saudi Arabia', 'Bangladesh', 'Russia', 'Hong Kong', 'Philippines', 'Egypt',
'Macedonia', 'Isle of Man', 'Lebanon', 'Bulgaria', 'Malaysia', 'Vanuatu', 'Peru',
'Vietnam'];

function strip(html) {
    var tmp = document.createElement('div');
    tmp.innerHTML = html;
    return (tmp.textContent || tmp.innerText).trim();
}

var list = Array.prototype.slice.call(document.querySelectorAll('section.job-tile'));
// var list = Array.prototype.slice.call(document.querySelectorAll('article.job-tile'));
list = list.map(function(row){
    var html = row.innerHTML;
    var numFreelancer = 1;

    var titleStartPos = html.indexOf('<a data-ng-bind-html');
    var titleEndPos = html.indexOf('</a>');
    var titleRaw = html.substring(titleStartPos, titleEndPos);
    var titleParsed = titleRaw.substring(titleRaw.indexOf('>') + 1, titleRaw.length);

    var hrefStartPos = titleRaw.indexOf('itemprop="url" href="');
    var hrefEndPos = titleRaw.indexOf('">');
    var hrefRaw = titleRaw.substring(hrefStartPos + 21, hrefEndPos);

    var descriptionStart = html.indexOf('<div data-eo-truncation-html-unsafe');
    var descriptionEnd = html.indexOf('less</a>');
    var descriptionRaw = html.substring(descriptionStart, descriptionEnd);

    var ds = descriptionRaw.indexOf('"truncatedHtml">');
    var de = descriptionRaw.lastIndexOf('</span>');
    descriptionRaw = descriptionRaw.substring(ds + 16, de);

    var isFixed = html.indexOf('<strong data-ng-if="::jsuJobTypeController.isFixed()" class="js-type">Fixed-Price</strong>');
    var hourType = 'hourly';
    var budget = 0;
    if (isFixed > -1 ) {
        hourType =  'fixed';
        var budgetStart = html.indexOf('itemprop="baseSalary">');
        var budgetRaw = '<span ' + html.substring(budgetStart, budgetStart + 40);
        budgetRaw = strip(budgetRaw);
        budgetRaw = budgetRaw.replace('$', '').replace(',', '');
        budget = parseFloat(budgetRaw.replace( /^\D+/g, ''));        
    }

    // var ps = html.indexOf('<time data-eo-relative="');
    // var pe = html.indexOf('" itemprop="datePosted"');
    // timeRaw = html.substring(ps + 24, pe);
    var ps = html.indexOf('<time data-eo-relative="');
    var pe = html.indexOf('" data-short');
    timeRaw = html.substring(ps + 24, pe);

    if (html.indexOf('<span class="text-muted">Number of freelancers needed:</span>') > 0) {
        numFreelancer = 2;
    }
    
    var countryStart = html.indexOf('<span class="glyphicon glyphicon-md air-icon-location m-0"></span>');
    var countryEnd = countryStart + 66 + 140;
    var countryRaw = html.substring(countryStart, countryEnd);

    var country = strip(countryRaw);

    return {title: titleParsed, type: hourType, budget: budget, time: timeRaw, url: hrefRaw, description: descriptionRaw, numFreelancer: numFreelancer, country: country};
}).filter(function(job){
    var country = ignoreCountry.indexOf(job.country) < 0;
    var freelancers = (job.numFreelancer == 1);
    var condition = freelancers && country;
    return condition;
});
console.log('end upwork script', list);

var ajax = {};
ajax.x = function () {
    if (typeof XMLHttpRequest !== 'undefined') {
        return new XMLHttpRequest();
    }
    var versions = [
        "MSXML2.XmlHttp.6.0",
        "MSXML2.XmlHttp.5.0",
        "MSXML2.XmlHttp.4.0",
        "MSXML2.XmlHttp.3.0",
        "MSXML2.XmlHttp.2.0",
        "Microsoft.XmlHttp"
    ];

    var xhr;
    for (var i = 0; i < versions.length; i++) {
        try {
            xhr = new ActiveXObject(versions[i]);
            break;
        } catch (e) {
        }
    }
    return xhr;
};

ajax.send = function (url, callback, method, data, async) {
    if (async === undefined) {
        async = true;
    }
    var x = ajax.x();
    x.open(method, url, async);
    x.onreadystatechange = function () {
        if (x.readyState == 4) {
            callback(x.responseText)
        }
    };
    if (method == 'POST') {
        x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    }
    x.send(data)
};

ajax.get = function (url, data, callback, async) {
    var query = [];
    for (var key in data) {
        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
    }
    ajax.send(url + (query.length ? '?' + query.join('&') : ''), callback, 'GET', null, async)
};

ajax.post = function (url, data, callback, async) {
    var query = [];
    for (var key in data) {
        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
    }
    ajax.send(url, callback, 'POST', query.join('&'), async)
};

ajax.post('https://upwork.manaknightdigital.com/api/upwork/posting', {
job_posting: JSON.stringify(list)
}, function(a) { console.log(a)});
            </textarea>
<br/>
        </div>
    </body>
</html>

