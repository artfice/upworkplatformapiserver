<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Upwork Proposal Dashboard</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.indigo-orange.min.css" />
<script defer src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    <style>
    iframe {
      height: 1000px;
      padding-top: 50px;
    }
    </style>
    </head>
    <body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header
            mdl-layout--fixed-tabs">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title">Upwork Dashboard</span>
</div>
    <!-- Tabs -->
    <div class="mdl-layout__tab-bar mdl-js-ripple-effect">
      <a href="#fixed-tab-1" class="mdl-layout__tab is-active">Jobs</a>
      <a href="#fixed-tab-2" class="mdl-layout__tab">Proposals</a>
      <a href="#fixed-tab-3" class="mdl-layout__tab">Responses</a>
      <a href="#fixed-tab-4" class="mdl-layout__tab">Search</a>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title">Menu</span>
  </div>
  <main class="mdl-layout__content">
    <section class="mdl-layout__tab-panel is-active" id="fixed-tab-1">
      <div class="page-content">
      <iframe src="https://upwork.manaknightdigital.com/upwork/proposal/dashboard"></iframe>
      </div>
    </section>
    <section class="mdl-layout__tab-panel" id="fixed-tab-2">
      <div class="page-content">
<iframe src="https://crossorigin.me/http://www.manaknight.com/proposal.php"></iframe>

      </div>
    </section>
    <section class="mdl-layout__tab-panel" id="fixed-tab-3">
      <div class="page-content">
      <iframe src="https://crossorigin.me/http://www.manaknight.com/responses.php"></iframe>

      </div>
    </section>
    <section class="mdl-layout__tab-panel" id="fixed-tab-4">
      <div class="page-content">
      <iframe src="https://upwork.manaknightdigital.com/upwork/proposal/search"></iframe>

      </div>
    </section>
  </main>
</div>
    </body>
</html>

