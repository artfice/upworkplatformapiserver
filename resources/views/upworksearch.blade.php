<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Upwork Proposal Dashboard</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.indigo-orange.min.css" />
<script defer src="https://code.getmdl.io/1.2.1/material.min.js"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }
            .pull-left {
                float: left;
            }
            .pull-right {
                float: right;
            }
            .clear {
                clear: both;
            }
            .proposal-container {
                width: 48%;
                float:left;
            }
            #proposal {
                width: 100%;
            }
            .proposal-url-container {
                width: 48%;
                float:right;
            }
            #proposal-urls {
                width: 100%;
            }
            .job-table table {
                border-collapse: collapse;
                width: 100%;
            }

            table td {
              padding-bottom: 10px;
              text-align: center;
            }

            table tr {
              border-bottom: 1px solid #e2e2e2;
            }
            #javascript {
                width: 100%;
                height: 200px;
            }
        </style>
    </head>
    <body>
        <form action="" method="POST">
 {{ csrf_field() }}
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="text" id="title" name="title">
    <label class="mdl-textfield__label" for="sample3">Title</label>
  </div><br/>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="text" id="url" name="url">
    <label class="mdl-textfield__label" for="sample3">URL</label>
  </div><br/>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="text" id="country" name="country">
    <label class="mdl-textfield__label" for="sample3">Country</label>
  </div><br/>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="text" id="category" name="category">
    <label class="mdl-textfield__label" for="sample3">Category</label>
  </div><br/>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="number" pattern="-?[0-9]*(\.[0-9]+)?" id="budget" name="budget">
    <label class="mdl-textfield__label" for="sample4">Budget</label>
    <span class="mdl-textfield__error">Input is not a Budget!</span>
  </div><br/>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="number" pattern="-?[0-9]*(\.[0-9]+)?" id="processed" name="processed">
    <label class="mdl-textfield__label" for="sample4">Processed</label>
    <span class="mdl-textfield__error">Input is not a Processed!</span>
  </div>
  <br/>
    <input type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" name="submit" value="Submit"/>
</form>
        <div class="job-table">
<br/>
            <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                <thead>
                <th>ID</th>
                <th>Title</th>
                <th>Type</th>
                <th>Budget</th>
                <th>Time</th>
                <th>Processed</th>
                <th>Description</th>
                </thead>
                <tbody>
                @foreach ($jobs as $job)
                    <tr>
                        <td>{{$job->id}}<br/>
<a class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" target="__blank" href="https://upwork.manaknightdigital.com/api/upwork/reprocess?id={{$job->id}}">Reprocess</a>
<br/><br/>
<a style="float:left" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" target="__blank" href="https://upwork.manaknightdigital.com/api/upwork/cancel?id={{$job->id}}">Ignore it</a>
                        </td>
                        <td><a  
                        target="__blank" 
                        href="https://www.upwork.com{{ $job->url }}"
                        >{{$job->title}}</a><br/>{{$job->url}}</td>
                        <td>{{$job->type == 'fixed' ? 'F' : 'H'}}</td>
                        <td>{{$job->budget}}</td>
                        <td>{!! str_replace(['+00:00', 'T'], ['', '<br/>'],$job->time) !!}
                        <br/>
                        {{$job->country}}
                        </td>
                        <td>{{$job->processed === 1 ? 'Y' : 'N'}}
                        <br/>
                        <a class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" target="__blank" href="https://upwork.manaknightdigital.com/api/upwork/process/report?id={{$job->id}}">Report</a>
                        </td>
                        <td>
                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onclick="document.getElementById('description-job-{{ $job->id }}').style = '';this.style='display:none';">Show</button>
                            <div id="description-job-{{ $job->id }}" style="display:none">{!! $job->description !!}</div>
                        </td>                    
                    </tr>
                @endforeach
                </tbody>
            </table>
<br/>
        </div>
    </body>
</html>