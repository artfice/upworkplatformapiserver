<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('upwork/proposal', 'UpworkDashboardController@mainIndex');
Route::get('upwork/proposal/dashboard', 'UpworkDashboardController@index');
Route::get('/upwork/proposal/search', 'UpworkDashboardController@search');
Route::post('/upwork/proposal/search', 'UpworkDashboardController@search');