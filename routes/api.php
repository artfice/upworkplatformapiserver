<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::post('upwork/posting', 'UpworkController@posting')->middleware('cors');
Route::get('upwork/posting2', 'UpworkController@posting2')->middleware('cors');
Route::get('upwork/process', 'UpworkController@processPostTest');
Route::get('upwork/reprocess', 'UpworkController@reprocessPost');
Route::get('upwork/cancel', 'UpworkController@cancelPost');
Route::get('upwork/process/report', 'UpworkController@processReport');
Route::get('upwork/category/report', 'UpworkController@categoryReport');